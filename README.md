# guestbook - Devops DO210322


# Run local
- sudo docker-compose up -d
- http://localhost:5000

[![pipeline status](https://gitlab.com/thieentv/guest-book/badges/master/pipeline.svg)](https://gitlab.com/thieentv/guest-book/-/commits/master)
# Push image to docker registry (gcr.io)
- gcloud auth login
- gcloud config set project devops-315509
- gcloud auth configure-docker
- docker tag guestbook-main_app gcr.io/devops-315509/guestbook-main_app:v1
- docker push gcr.io/devops-315509/guestbook-main_app:v1

- kubectl apply -f app.yaml
- kubectl get pods
- kubectl get service
- k get pods frontend-5bd556cb45-8cgsj -o yaml
- kubectl logs deployment/frontend
- kubectl delete ns guestbook-production
- kubectl create namespace guestbook-production
- 
# Create namespace if not exist
- kubkubectl config set-context --current --namespace=guestbook-productionectl create namespace guestbook-production --dry-run=client -o yaml | kubectl apply -f -

# Login google cloud
- https://github.com/JanMikes/gitlab-ci-push-to-gcr
- https://kartaca.com/en/deploy-your-application-directly-from-gitlab-ci-to-google-kubernetes-enginegke/

# Auto update latest image
- kubectl rollout restart deployment/frontend

# Production url
- http://35.185.148.219/ 

# Staging url
- http://35.229.251.197/

# Develop url
- http://35.229.237.103/
